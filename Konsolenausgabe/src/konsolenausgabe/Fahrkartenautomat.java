﻿package konsolenausgabe;
import java.util.*;

class Fahrkartenautomat
{
    public static void main(String[] args)
    { 
       double eingezahlterGesamtbetrag = 0.0;
       double rückgabebetrag = 0.0;
       int millisekunde = 250;
       String einheit = "EURO";
       String einheit2 = "CENT";
       
       // Fahrkartenbestellung
       // -----------
       double zuZahlenderBetrag = fahrkartenbestellungErfassen();
       
       // Geldeinwurf
       // -----------
       
       rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

       // Fahrscheinausgabe
       // -----------------
       
       warte(millisekunde);	
       
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       
       muenzeAusgeben(rückgabebetrag, einheit, einheit2);
       
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
       
    }
    
    private static double fahrkartenbestellungErfassen()
    {
    	Scanner tastatur = new Scanner(System.in);
    	double fahrkartenKosten = 0.0;
    	double zuZahlenderBetrag = 0.0;
    	int anzahlderFahrkarten = 0;
    	
    	 System.out.print("Zu zahlender Betrag (EURO): ");
    	 fahrkartenKosten = tastatur.nextDouble();
         
         System.out.print("Anzahl der Fahrkarten: ");
         anzahlderFahrkarten = tastatur.nextInt();
         
         zuZahlenderBetrag = (fahrkartenKosten * (double)anzahlderFahrkarten);
         zuZahlenderBetrag = rundung(zuZahlenderBetrag, 4);
    	
         return zuZahlenderBetrag;
    }
    
    private static double fahrkartenBezahlen(double zuZahlen)
    {
    	Scanner tastatur = new Scanner(System.in);
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneMünze = 0.0;
    	double rückgabebetrag = 0.0;
    	
    	while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	rückgabebetrag = eingezahlterGesamtbetrag - zuZahlen;
    	
    	return rückgabebetrag;
    }
    
    private static void warte (int millisekunde)
    {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(millisekunde);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    
    private static void muenzeAusgeben(double rückgabebetrag, String einheit, String einheit2)
    {
    	if(rückgabebetrag >= 0.0)
	       {
	           System.out.printf("%s %.2f %s %n","Der Rückgabebetrag in Höhe von ", rückgabebetrag, " EURO");
	           System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	              System.out.println("2" + einheit);
	              rückgabebetrag -= 2.0;
	              rückgabebetrag = rundung(rückgabebetrag, 4);
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	              System.out.println("1" + einheit);
	              rückgabebetrag -= 1.0;
	              rückgabebetrag = rundung(rückgabebetrag, 4);
	           }
	           while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
	           {
	              System.out.println("50" + einheit2);
	              rückgabebetrag -= 0.50;
	              rückgabebetrag = rundung(rückgabebetrag, 4);
	           }
	           while(rückgabebetrag >= 0.20) // 20 CENT-Münzen
	           {
	              System.out.println("20" + einheit2);
	              rückgabebetrag -= 0.20;
	              rückgabebetrag = rundung(rückgabebetrag, 4);
	           }
	           while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
	           {
	              System.out.println("10" + einheit2);
	              rückgabebetrag -= 0.10;
	              rückgabebetrag = rundung(rückgabebetrag, 4);
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	              System.out.println("5" + einheit2);
	              rückgabebetrag -= 0.05;
	              rückgabebetrag = rundung(rückgabebetrag, 4);
	           }
	       }
    }
    
    private static double rundung(double Wert, int Nachkomma)
    {
    	double retVal = 0.0;
    	double Komma = 1.0;
    	
    	if (Nachkomma > 0)
    	{
    		for (int i = 0; i < Nachkomma; i++) 
    		{
    			Komma = Komma * 10;
    		}
    	}
    	retVal = Math.round(Wert * Komma) / Komma;
    	return retVal;
    }
}